package com.inter.test.Util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

public class Constants {
    public static String loadJSONFromAsset(String fileName , Context context) {
        String json = null;
        try {

            AssetManager am = context.getAssets();
            InputStream is = am.open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
