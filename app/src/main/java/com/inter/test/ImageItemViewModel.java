package com.inter.test;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.inter.test.Model.ItemModel;
import com.squareup.picasso.Picasso;

public class ImageItemViewModel extends BaseObservable {


    ItemModel item;
    static Context context;
    public ImageItemViewModel(ItemModel item , Context context)
    {
        this.item = item;
        this.context = context;

    }

    @Bindable
    public String getLike()
    {
        return "Likes: "+item.getLikes();
    }

    @Bindable
    public String getPicture()
    {
        return item.getUrl();
    }

    @BindingAdapter("setPicture")
    public static void setPicture(ImageView imageView , String Picture)
    {
        Glide.with(context).load(Picture).into(imageView);
        //Picasso.get().load(Picture).into(imageView);
    }

}
