package com.inter.test;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.inter.test.Adapter.ItemAdapter;
import com.inter.test.Model.ItemModel;
import com.inter.test.R;
import com.inter.test.Util.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivityViewModel extends BaseObservable {

    ArrayList<ItemModel> list ;
    boolean isRefresh = false;

    public boolean isMoveToTop() {
        return MoveToTop;
    }
    @Bindable
    public void setMoveToTop(boolean moveToTop) {
        MoveToTop = moveToTop;
    }

    boolean MoveToTop = false;
    @Bindable
    public boolean isRefresh() {
        return isRefresh;
    }

    public void setRefresh(boolean refresh) {
        isRefresh = refresh;
    }

    static Context myContext;
    public MainActivityViewModel(Context context)
    {
        this.myContext = context;
        list = PopulateData();
    }

    ArrayList<ItemModel> PopulateData()
    {
        ArrayList<ItemModel> list = new ArrayList<>();
      //  String data = String.valueOf(myContext.getResources().getString(R.string.data));
        try
        {
           // JSONObject bb = new JSONObject(data);
            JSONArray array = new JSONArray(Constants.loadJSONFromAsset("data.json" ,myContext));
            for(int i=0 ; i < array.length(); i++)
            {
                JSONObject item = array.getJSONObject(i);
                String likes = item.get("likes").toString();
                JSONObject imageURLS = item.getJSONObject("urls");
                String url = imageURLS.get("raw").toString();
                ItemModel model = new ItemModel();
                model.setLikes(likes);
                model.setUrl(url);
                list.add(model);
            }
        }
        catch (Exception e)
        {
            Log.e("Error" , "Error" , e);

        }
        return list;
    }



    @Bindable
    public ArrayList<ItemModel> getData()
    {
        return list;
    }

    @BindingAdapter("setAdapter")
    public static void setAdapter(RecyclerView recyclerView , ArrayList<ItemModel> list)
    {
        ItemAdapter adapter = new ItemAdapter(myContext , list);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));
        recyclerView.setAdapter(adapter);

    }

    public void reloadData()
    {
        isRefresh = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                isRefresh = false;
                notifyPropertyChanged(BR.refresh);
            }
        },1000);
    }

    @BindingAdapter("scrollToTop")
    public static void scrollToTop(RecyclerView view , boolean isTop)
    {
        if(isTop)
        {
            
            view.scrollToPosition(0);
        }
    }
    public View.OnClickListener onFloatingButtonClicked()
    {
        return view ->{
                        setMoveToTop(true);
                        notifyPropertyChanged(BR.data);
        };
    }
}
