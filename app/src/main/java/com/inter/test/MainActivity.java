package com.inter.test;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.inter.test.databinding.ActivityMainBinding;

import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this , R.layout.activity_main);
        MainActivityViewModel model = new MainActivityViewModel(this);
        binding.setMainModel(model);


    }
}
