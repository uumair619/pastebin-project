package com.inter.test.Adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.inter.test.ImageItemViewModel;
import com.inter.test.Model.ItemModel;
import com.inter.test.R;
import com.inter.test.databinding.ItemsViewBinding;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder>{

    ArrayList<ItemModel> ItemList = new ArrayList<>();
    Context myContext ;

    public ItemAdapter(Context context , ArrayList<ItemModel> ItemList)
    {
        this.ItemList = ItemList;
        myContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(myContext);
        ItemsViewBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.items_view, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ItemModel object = ItemList.get(position);
        ImageItemViewModel model = new ImageItemViewModel(object , myContext);
        holder.bind(model);

    }
    @Override
    public int getItemCount() {
        return ItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
       // private final ItemsViewBinding binding;
        private final ItemsViewBinding binding;
        public MyViewHolder(ItemsViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


        }
        public void bind(ImageItemViewModel obj) {
            binding.setModel(obj);
            binding.executePendingBindings();
        }
    }
}
